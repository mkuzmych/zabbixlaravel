# Laravel library for Zabbix api

## Installation

You can install the package via composer:

```bash
composer require mkuzmych/zabbixlaravel
```
## Configuration

Laravel Zabbix requires connection configuration. To get started, you'll need to publish all vendor assets:

```bash
$ php artisan vendor:publish --provider="Mkuzmych\Zabbixlaravel\ZabbixlaravelServiceProvider"
```

This will create a `config/zabbix.php` file in your app that you can modify to set your configuration. Also, make sure you check for changes to the original config file in this package between releases.

You are free to change the configuration file as needed. In envorement you can set:

```php
ZABBIX_APIURL="http://company.com/api_jsonrpc.php"
ZABBIX_USER="user"
ZABBIX_PASSWORD="password"
ZABBIX_HTTPUSER="httpuser"
ZABBIX_HTTPPASSWORD="httppassword"
ZABBIX_AUTHTOKEN=
```

## Usage

Use the library as Facade in Laravel.

``` php
\Zabbixlaravel::request('host.get', array(
				'with_items' => true,
				'selectItems' => true
			));

\Zabbixlaravel::getAuthToken();
```

## Documentation

There are other classes in this package that are not documented here. This is because the package is a Laravel wrapper of [the Zabbix PHP package](https://gitlab.com/mkuzmych/zabbix).

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Troubleshooting

If you have any questions or problems, create a [ticket](https://gitlab.com/mkuzmych/zabbixlaravel/-/issues).

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.