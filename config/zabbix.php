<?php

/*
 * You can place your custom package configuration in here.
 */
return [

    /*
     |--------------------------------------------------------------------------
     | Zabbix API URL Settings
     |--------------------------------------------------------------------------
     | http://company.com/zabbix/api_jsonrpc.php
     */
    'apiurl' => env('ZABBIX_APIURL', null),

    /*
     |--------------------------------------------------------------------------
     | Zabbix User
     |--------------------------------------------------------------------------
     */
    'user' => env('ZABBIX_USER', null),

    /*
     |--------------------------------------------------------------------------
     | Zabbix Password
     |--------------------------------------------------------------------------
     */
    'password' => env('ZABBIX_PASSWORD', null),

    /*
     |--------------------------------------------------------------------------
     | Zabbix HTTP auth user
     |--------------------------------------------------------------------------
     */
    'httpuser' => env('ZABBIX_HTTPUSER', null),

    /*
     |--------------------------------------------------------------------------
     | Zabbix HTTP auth user
     |--------------------------------------------------------------------------
     */
    'httppassword' => env('ZABBIX_HTTPPASSWORD', null),

    /*
     |--------------------------------------------------------------------------
     | Zabbix Auth Token
     |--------------------------------------------------------------------------
     |  If you want to use an already issued auth token instead of username
     |  and password.
     */
    'authToken' => env('ZABBIX_AUTHTOKEN', ''),

    /*
     |--------------------------------------------------------------------------
     | Zabbix stream context
     |--------------------------------------------------------------------------
     | 'streamcontext' => [
	 |    	'ssl' => array( // set ssl skip cert
     |           "verify_peer"=>false,
     |           "verify_peer_name"=>false,
     |       ),
     |       'http' => array( // set proxy
     |           'proxy' => 'tcp://X.X.X.X:3128',
     |           'request_fulluri' => true,
     |       ),
     |  ],
     */
     'streamcontext' => [
	     	'ssl' => array( // set ssl skip cert
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            )
     ],
];