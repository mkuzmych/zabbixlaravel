<?php

namespace Mkuzmych\Zabbixlaravel;

use Illuminate\Support\ServiceProvider;

class ZabbixlaravelServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        $this->publishes([
        __DIR__.'/../config/zabbix.php' => config_path('zabbix.php')
        ], 'config');
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/zabbix.php', 'zabbix');

        // Register the main class to use with the facade
        $this->app->singleton('zabbixlaravel', function () {
            return new Zabbixlaravel;
        });
    }
}
