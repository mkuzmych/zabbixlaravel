<?php

namespace Mkuzmych\Zabbixlaravel;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Mkuzmych\Zabbixlaravel\Skeleton\SkeletonClass
 */
class ZabbixlaravelFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'zabbixlaravel';
    }
}
