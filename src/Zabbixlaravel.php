<?php

namespace Mkuzmych\Zabbixlaravel;

class Zabbixlaravel
{
    private $api;

    public function __construct(){
    	
    	$this->api = new \Zabbix\Zabbix(config('zabbix.apiurl'),config('zabbix.user'),config('zabbix.password'),config('zabbix.httpuser'),config('zabbix.httppassword'),config('zabbix.authToken'),config('zabbix.streamcontext'));

    }

    /**
     * Get auth tken for request
     */
    public function getAuthToken(string $authToken = null)
    {
    	return $this->api->getAuthToken($authToken);
    }

    /**
     * Invalidate token
     */
    public function userLogout(string $authToken)
    {
    	return $this->api->userLogout($authToken);
    }

    /**
	 * Make request for Zabbix api
	 *
	 * @param string $method https://www.zabbix.com/documentation
	 * @param array $params params for request
	 * @param boolean $withAuth need token to request
	 */

    public function request(string $method, array $params = [], $withAuth = true)
    {
    	return $this->api->request($method, $params, $withAuth);
	}
}
